sap.ui.define([
		"sap/ui/model/json/JSONModel",
		"sap/ui/Device"
	], function (JSONModel, Device) {
		"use strict";

		return {
			createDeviceModel : function () {
				var oModel = new JSONModel(Device);
				oModel.setDefaultBindingMode("OneWay");
				return oModel;
			},

			createFLPModel : function () {
				var fnGetuser = jQuery.sap.getObject("sap.ushell.Container.getUser"),
					bIsShareInJamActive = fnGetuser ? fnGetuser().isJamActive() : false,
					oModel = new JSONModel({
						isShareInJamActive: bIsShareInJamActive
					});
				oModel.setDefaultBindingMode("OneWay");
				return oModel;
			},
			
			createViewModelParticipants: function() {
				var oModel = new sap.ui.model.json.JSONModel({
				ParticipantsCount: 0,
				WatchParticipant: false,
				Called: false,
				ShowFullName: false,
				ShowFirstName: true
			});
			
			return oModel;
			},
			
			createUploadModel: function() {
				var oModel = new sap.ui.model.json.JSONModel({
				Photo: "",
				UpdateProfilePicture: "notUploaded"
			});
			
			return oModel;
			}
		};

	}
);