sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/ui/layout/HorizontalLayout",
	"sap/ui/layout/VerticalLayout",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/Label",
	"sap/m/MessageToast",
	"sap/m/Text",
	"sap/m/TextArea",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/ui/core/format/DateFormat",
	"com/sap/build/standard/untitledPrototype/model/models"
], function (BaseController, MessageBox, Dialog2, Utilities, History, DateFormat, models) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.untitledPrototype.controller.SingleParticipant", {
		handleRouteMatched: function (oEvent) {
			var oParams = oEvent.getParameters();
			this.currentRouteName = oParams.name;
			var sContext;
			if (oParams.arguments.midContext) {
				sContext = oParams.arguments.midContext;
			} else {
				if (this.getOwnerComponent().getComponentData()) {
					var patternConvert = function (oParam) {
						if (Object.keys(oParam).length !== 0) {
							for (var prop in oParam) {
								if (prop !== "sourcePrototype") {
									return prop + "(" + oParam[prop][0] + ")";
								}
							}
						}
					};

					sContext = patternConvert(this.getOwnerComponent().getComponentData().startupParameters);
				}
			}
			var sContextModelProperty = "/midContext";

			if (sContext) {

				var oPath = {
					path: "/" + sContext,
					parameters: {}
				};

				var contextPath = '/' + sContext;
				var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

				//this.getView().bindObject(oPath);
				this.oFclModel.setProperty(sContextModelProperty, sContext);
				//this.getView().setBindingContext(givenContext);

				this.getView().setBindingContext(givenContext);
				this.getView().bindElement(contextPath);

			}

			var pageName = this.oView.sViewName.split('.');
			pageName = pageName[pageName.length - 1];

			if (pageName === this.currentRouteName) {
				this.oView.getModel('fclButton').setProperty('/visible', true);
			} else {
				this.oView.getModel('fclButton').setProperty('/visible', false);
			}
			
			// test

			if (oEvent.mParameters.arguments.layout && oEvent.mParameters.arguments.layout.includes('FullScreen')) {
				this.oFclModel.setProperty('/expandIcon/img', 'sap-icon://exit-full-screen');
				this.oFclModel.setProperty('/expandIcon/tooltip', 'Exit Full Screen Mode');
			} else {
				this.oFclModel.setProperty('/expandIcon/img', 'sap-icon://full-screen');
				this.oFclModel.setProperty('/expandIcon/tooltip', 'Enter Full Screen Mode');
			}

			this.setInputEvents();

			this.createUploadPhotoDialog();
			
			
			// update updateModel in order not to show the same uplaoded image for others
			var uploadModel = this.getView().getModel("uploadModel");
			if (uploadModel.getData().UpdateProfilePicture !== "notUploaded") {

			
				uploadModel.getData().UpdateProfilePicture = "notUploaded";
				uploadModel.refresh();
			}

		},
		
		isUserALeader: function(role) {
			if(role === "MED"){
				return false;
			}else{
				return true;
			}
		},
		
		sendEmail:function(oEvent){
		 sap.m.URLHelper.triggerEmail(oEvent.getSource().getText(), "", "");
		},
		
		callPhoneNumber: function(oEvent) {
			sap.m.URLHelper.triggerTel(sap.m.URLHelper.triggerTel);
		},
		
		setInputEvents: function () {
	
		
		},

		_onExpandButtonPress: function () {
			var endColumn = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().columnsVisibility.endColumn;
			var isFullScreen = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().isFullScreen;
			var nextLayout;
			var actionsButtonsInfo = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().actionButtonsInfo;
			if (endColumn && isFullScreen) {
				nextLayout = actionsButtonsInfo.endColumn.exitFullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(2).layout;
			}
			if (!endColumn && isFullScreen) {
				nextLayout = actionsButtonsInfo.midColumn.exitFullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(1).layout;
			}
			if (endColumn && !isFullScreen) {
				nextLayout = actionsButtonsInfo.endColumn.fullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(3).layout;
			}
			if (!endColumn && !isFullScreen) {
				nextLayout = actionsButtonsInfo.midColumn.fullScreen;
				nextLayout = nextLayout ? nextLayout : 'MidColumnFullScreen'
			}
			var pageName = this.oView.sViewName.split('.');
			pageName = pageName[pageName.length - 1];
			this.oRouter.navTo(pageName, {
				layout: nextLayout
			});

		},
		_onCloseButtonPress: function () {
			var endColumn = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().columnsVisibility.endColumn;
			var nextPage;
			var nextLevel = 0;

			var actionsButtonsInfo = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().actionButtonsInfo;

			var nextLayout = actionsButtonsInfo.midColumn.closeColumn;
			nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(0).layout;

			if (endColumn) {
				nextLevel = 1;
				nextLayout = actionsButtonsInfo.endColumn.closeColumn;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(1).layout;
			}

			var pageName = this.oView.sViewName.split('.');
			pageName = pageName[pageName.length - 1];
			var routePattern = this.oRouter.getRoute(pageName).getPattern().split('/');
			var contextFilter = new RegExp('^:.+:$');
			var pagePattern = routePattern.filter(function (pattern) {
				var contextPattern = pattern.match(contextFilter);
				return contextPattern === null || contextPattern === undefined;
			});

			var nextPage = pagePattern[nextLevel];
			this.oRouter.navTo(nextPage, {
				layout: nextLayout
			});

		},
		onInit: function () {

			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.attachRouteMatched(this.handleRouteMatched, this);
			this.oFclModel = this.getOwnerComponent().getModel("FclRouter");
			this.oFclModel.setProperty('/targetAggregation', 'midColumnPages');
			this.oFclModel.setProperty('/expandIcon', {});
			this.oView.setModel(new sap.ui.model.json.JSONModel({}), 'fclButton');

		},

		countLendings: function (oEvent) {
			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().LendingsCount = oEvent.getParameter("total");
			viewModel.refresh();
		},

		formatDate: function (date) {
			if (date !== null) {
				var day = date.getDate();
				day = day.toString().length < 2 ? "0" + day : day;
				var month = parseInt(date.getMonth()) + 1;
				month = month.toString().length < 2 ? "0" + month : month;
				var year = date.getFullYear();

				var string = day + "/" + month + "/" + year;
				return string;
			}
		},

		formatBirthday: function (date) {

			if (date !== undefined && date !== null) {

				var day = date.getDate();
				day = day.toString().length < 2 ? "0" + day : day;
				var month = parseInt(date.getMonth()) + 1;
				month = month.toString().length < 2 ? "0" + month : month;
				var year = date.getFullYear();

				var string = day + "." + month + "." + year;
				return string;
			}
		},

		_onPressSave: function () {
			this.getView().setBusy(true);
			this.getView().getModel().submitChanges({
				success: function (oData, response) {
					this.getView().setBusy(false);
					MessageBox.success("Data er gemt succesfuldt.");
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
		},

		errorCallBackShowInPopUp: function (oError) {
			if (oError) {
				if (oError.responseText) {
					var errorCode = JSON.parse(oError.responseText).error.code;
					var errorMessage = JSON.parse(oError.responseText).error.message.value;

					if (sap.hybrid) {
						//Log application error
						sap.Logger.error(errorCode + " " + errorMessage);
					}

					MessageBox.error("Fejl" + ": " + errorCode + " - " + errorMessage);
				} else {
					if (sap.hybrid) {
						//Log application error
						sap.Logger.error(oError.message);
					}

					MessageBox.error("Fejl" + ": " + oError.message);
				}
			}
		},

		formatImage: function (updateProfilePicture, name) {
			if (updateProfilePicture !== "notUploaded") {
				return updateProfilePicture;
			}

			// we only use name in order to call function
			if (name !== null && name !== undefined) {
var persno = this.getView().getBindingContext().getObject().Persno;// __metadata.media_src;
			var path = this.getView().getModel().sServiceUrl + "/ParticipantImageSet('" + persno + "')/$value";
				if (!this.getView().getModel("device").getData().isHybridApp) {
					path = path.replace(/^.*\/\/[^\/]+/, '');
				}

				this.getView().getModel().refresh();

				return path;
			}
		},

		createUploadPhotoDialog: function () {
			var oView = this.getView();
			var uploadProfilePictureDialog = oView.byId("uploadProfilePictureDialog");
			// create dialog lazily
			if (!uploadProfilePictureDialog) {
				// create dialog via fragment factory
				uploadProfilePictureDialog = sap.ui.xmlfragment(oView.getId(),
					"com.sap.build.standard.untitledPrototype.view.fragments.UploadPhoto",
					this);
				oView.addDependent(uploadProfilePictureDialog);
				uploadProfilePictureDialog.setBusyIndicatorDelay(0);
			}
		},

		openUploadPhotoDialog: function () {
			var uploadProfilePictureDialog = this.getView().byId("uploadProfilePictureDialog");
			if (uploadProfilePictureDialog) {
				uploadProfilePictureDialog.setBusy(false);
				uploadProfilePictureDialog.open();
			}
		},

		closeUploadPhotoDialog: function () {
			var uploadProfilePictureDialog = this.getView().byId("uploadProfilePictureDialog");
			if (uploadProfilePictureDialog) {
				uploadProfilePictureDialog.setBusy(false);
				uploadProfilePictureDialog.close();
			}
		},

		onPhotoSelected: function (oEvent) {
			var uploadModel = this.getView().getModel("uploadModel");
			uploadModel.getData().Photo = oEvent.getParameter("files")[0];
			uploadModel.refresh();
			this.convertToBase64(uploadModel.getData().Photo);
		},

		convertToBase64: function (selectedFile) {
			var reader = new FileReader();
			reader.onloadend = function (e) {
				//callback(e.target.result, e.target.error);
				// convert imageBase64 string

				var uploadModel = this.getView().getModel("uploadModel");
				uploadModel.getData().UpdateProfilePictureTemp = e.target.result;
				uploadModel.refresh();
			}.bind(this);
			reader.readAsDataURL(selectedFile);
		},

		onPressUpload: function () {
			var oFileUploader = this.getView().byId("fileUploader");
			var serviceUrl = this.getView().getModel().sServiceUrl + "/ParticipantSet";

			oFileUploader.setUploadUrl(serviceUrl);
			if (oFileUploader.getValue() === "") {
				//MessageToast.show("Please Choose any File”);
			}
			oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
				name: "SLUG",
				value: this.getView().getBindingContext().getObject().Persno
			}));

			oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
				name: "x-csrf-token",
				value: this.getView().getModel().getSecurityToken()
			}));
			oFileUploader.setUseMultipart(false);
			oFileUploader.setSendXHR(true);
			oFileUploader.upload();

			// show new photo
			var uploadModel = this.getView().getModel("uploadModel");
			uploadModel.getData().UpdateProfilePicture = uploadModel.getData().UpdateProfilePictureTemp;
			uploadModel.refresh();

			oFileUploader.destroyHeaderParameters();

			// clear string in input field
			oFileUploader.setValue("");

			this.closeUploadPhotoDialog();

		},

		//Before upload started
		onUploadStarted: function (oControlEvent) {
			//	sap.ui.core.BusyIndicator.show();
		},

		onUploadCompleteNotWorking: function (oControlEvent) {

			if (this.uploadStarted) {
				sap.ui.core.BusyIndicator.hide();
				var oFileUploader = this.getView().byId("fileUploader");
				oFileUploader.destroyHeaderParameters();

				this.closeUploadPhotoDialog();

				this.uploadStarted = false;
			}

		}

	});
}, /* bExport= */ true);