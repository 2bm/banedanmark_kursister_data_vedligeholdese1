sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/ui/model/Sorter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, MessageBox, Utilities, History, Sorter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.untitledPrototype.controller.Page1", {
		handleRouteMatched: function (oEvent) {
			var oParams = oEvent.getParameters();
			this.currentRouteName = oParams.name;
			var sContext;

			if (oParams.arguments.beginContext) {
				sContext = oParams.arguments.beginContext;
			} else {
				if (this.getOwnerComponent().getComponentData()) {
					var patternConvert = function (oParam) {
						if (Object.keys(oParam).length !== 0) {
							for (var prop in oParam) {
								if (prop !== "sourcePrototype") {
									return prop + "(" + oParam[prop][0] + ")";
								}
							}
						}
					};

				//(	sContext = patternConvert(this.getOwnerComponent().getComponentData().startupParameters);
				}
			}
			var sContextModelProperty = "/";

			if (sContext) {

				var oPath = {
					path: "/" + sContext,
					parameters: {}
				};

				this.getView().bindObject(oPath);

				this.oFclModel.setProperty(sContextModelProperty, sContext);
			}

			this.oView.getModel("fclButton").setProperty("/visible", false);

			if (oEvent.mParameters.arguments.layout && oEvent.mParameters.arguments.layout.includes("FullScreen")) {
				this.oFclModel.setProperty("/expandIcon/img", "sap-icon://exit-full-screen");
				this.oFclModel.setProperty("/expandIcon/tooltip", "Exit Full Screen Mode");
			} else {
				this.oFclModel.setProperty("/expandIcon/img", "sap-icon://full-screen");
				this.oFclModel.setProperty("/expandIcon/tooltip", "Enter Full Screen Mode");
			}

			if (this.getView().getModel("participantTableModel") === undefined) {
				this.createParticipantTableModel();
			}

			this.adjustFrontendToMobileAndFirstColumnActive();

			this.getUserInfoSet();
		},

		getUserInfoSet: function () {
				var viewModel = this.getView().getModel("viewModelParticipants");
			if (this.getOwnerComponent().getComponentData().startupParameters.Role !== undefined) {
					viewModel.getData().Role = this.getOwnerComponent().getComponentData( ).startupParameters.Role[0];
			}
			this.getView().getModel().read("/UserInfoSet", {
				success: function (oData) {
					viewModel = this.getView().getModel("viewModelParticipants");
					viewModel.getData().Persno = oData.results[0].Persno;
					//viewModel.getData().Role = oData.results[0].Role;

					viewModel.refresh();

					if (viewModel.getData().Role === "MED" && viewModel.getData().Persno !== "") {
						this.showMessageBoxForUser(viewModel.getData().Persno);
					}
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
		},

		showMessageBoxForUser: function (persno) {
				this.filterTableToSingleParticipant(persno);
							var contextPath = "/ParticipantSet('" + persno + "')";

							var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

							this.doNavigate("SingleParticipant", givenContext, "", "", 0);
							
								this.messageBoxShownForUser = true;
			if (this.messageBoxShownForUser === undefined) {
				var messageBoxText = "Du er logget ind med medarbejdernr.: " + persno +
					". \n \n Du bliver nu videreført til dine medarbejderdata.";

				MessageBox.show(messageBoxText, {
					icon: sap.m.MessageBox.Icon.INFORMATION,
					title: "Information",
					actions: [sap.m.MessageBox.Action.OK],
					defaultAction: sap.m.MessageBox.Action.OK,
					styleClass: this.getView().$().closest(".sapUiSizeCompact").length ? "sapUiSizeCompact" : "",
					onClose: function (oAction, object) {
						if (oAction === sap.m.MessageBox.Action.OK) {
							
							this.filterTableToSingleParticipant(persno);
							var contextPath = "/ParticipantSet('" + persno + "')";

							var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

							this.doNavigate("SingleParticipant", givenContext, "", "", 0);

						}
					}.bind(this)
				});
				this.messageBoxShownForUser = true;
			}
		},
		
		filterTableToSingleParticipant: function(persno) {
			var sControlId = "sap_List_Page_0-content-build_simple_Table-1599115124519";
						var oControl = this.getView().byId(sControlId);
						//var aFinalFilters = [];

						var aFilters = [];
						aFilters.push(new sap.ui.model.Filter("Persno", sap.ui.model.FilterOperator.Contains, parseInt(persno)));
						//aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, false)] : [];
						/*var oBindingOptions = this.updateBindingOptions(sControlId, {
							filters: aFinalFilters
						});*/
						var oBindingInfo = oControl.getBindingInfo("items");
						if (oBindingInfo) {
							oControl.bindAggregation("items", {
								model: oBindingInfo.model,
								path: oBindingInfo.path,
								parameters: oBindingInfo.parameters,
								template: oBindingInfo.template,
								templateShareable: true,
								filters: aFilters
							});
						}
		},

		showExtractReport: function (role) {
			if (role === "LED" || role === undefined) {
				return false;
			} else {
				return true;
			}
		},
/*
		adjustFrontendToMobileAndFirstColumnActive: function () {
			// ændre fornavne til navn hvis denne er true ellers fornavne
			var runningOnPhone = sap.ui.Device.system.phone;

			// check if first Column is Active
			var firstColumnActive = $("#application-BUILD-prototype-component---app--idAppControl-beginColumn").hasClass(
				"sapFFCLColumnFirstActive") ? true : false;

			if (runningOnPhone || firstColumnActive) {
				// change column name
				this.getView().getModel("participantTableModel").getData().ColumnName = "Navn";
				this.getView().getModel("participantTableModel").refresh(true);

				// change data to be shown
				this.getView().getModel("viewModelParticipants").getData().ShowFirstName = false;
				this.getView().getModel("viewModelParticipants").getData().ShowFullName = true;
				this.getView().getModel("viewModelParticipants").refresh(true);

			} else {

				// change column name
				this.getView().getModel("participantTableModel").getData().ColumnName = "Fornavn";
				this.getView().getModel("participantTableModel").refresh(true);

				// change data to be shown	
				this.getView().getModel("viewModelParticipants").getData().ShowFirstName = true;
				this.getView().getModel("viewModelParticipants").getData().ShowFullName = false;
				this.getView().getModel("viewModelParticipants").refresh(true);

			}
		},*/

		createParticipantTableModel: function () {
			this.getView().setModel(new sap.ui.model.json.JSONModel({
				ColumnName: "Fornavn"
			}), "participantTableModel");

		},

		doNavigate: function (sRouteName, oBindingContext, fnPromiseResolve, sViaRelation, iNextLevel) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var routePattern = this.oRouter.getRoute(sRouteName).getPattern().split("/");
			var contextFilter = new RegExp("^:.+:$");
			var pagePattern = routePattern.filter(function (pattern) {
				var contextPattern = pattern.match(contextFilter);
				return contextPattern === null || contextPattern === undefined;
			});
			iNextLevel = iNextLevel !== undefined ? iNextLevel : pagePattern ? pagePattern.length - 1 : 0;
			this.oFclModel = this.oFclModel ? this.oFclModel : this.getOwnerComponent().getModel("FclRouter");

			var sEntityNameSet;
			var oNextUIState = this.getOwnerComponent().getSemanticHelper().getNextUIState(iNextLevel);
			var sBeginContext, sMidContext, sEndContext;
			if (iNextLevel === 0) {
				sMidContext = sPath;
			}

			if (iNextLevel === 1) {
				sBeginContext = this.oFclModel.getProperty("/beginContext");
				sMidContext = sPath;
			}

			if (iNextLevel === 2) {
				sBeginContext = this.oFclModel.getProperty("/beginContext");
				sMidContext = this.oFclModel.getProperty("/midContext");
				sEndContext = sPath;
			}

			var sNextLayout = oNextUIState.layout;

			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
					if (iNextLevel === 0) {
						sBeginContext = sPath;
					} else if (iNextLevel === 1) {
						sMidContext = sPath;
					} else {
						sEndContext = sPath;
					}
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						beginContext: sBeginContext,
						midContext: sMidContext,
						endContext: sEndContext,
						layout: sNextLayout
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function (bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}
						if (iNextLevel === 0) {
							sBeginContext = sPath;
						} else if (iNextLevel === 1) {
							sMidContext = sPath;
						} else {
							sEndContext = sPath;
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName, {
								layout: sNextLayout
							});
						} else {
							this.oRouter.navTo(sRouteName, {
								beginContext: sBeginContext,
								midContext: sMidContext,
								endContext: sEndContext,
								layout: sNextLayout
							}, false);
						}
					}.bind(this));
				}
			} else {
				if (sMidContext.includes("/")) {
					sMidContext = sMidContext.substring(1);
				}

				this.oRouter.navTo(sRouteName, {
					layout: sNextLayout,
					midContext: sMidContext
				});
			}

			if (typeof fnPromiseResolve === "function") {

				fnPromiseResolve();
			}

		},
		
		adjustFrontendToMobileAndFirstColumnActive: function () {
			// ændre fornavne til navn hvis denne er true ellers fornavne
			//var runningOnPhone = sap.ui.Device.system.phone;

			// check if first Column is Active
			var firstColumnActive = $("#application-BUILD-prototype-component---app--idAppControl-beginColumn").hasClass(
				"sapFFCLColumnFirstActive") ? true : false;

			if (firstColumnActive) {
				// change column name
				this.getView().getModel("participantTableModel").getData().ColumnName = "Navn";
				this.getView().getModel("participantTableModel").refresh(true);

				// change data to be shown
				this.getView().getModel("viewModelParticipants").getData().ShowFirstName = false;
				this.getView().getModel("viewModelParticipants").getData().ShowFullName = true;
				this.getView().getModel("viewModelParticipants").refresh(true);

			} else {

				// change column name
				this.getView().getModel("participantTableModel").getData().ColumnName = "Fornavn";
				this.getView().getModel("participantTableModel").refresh(true);

				// change data to be shown	
				this.getView().getModel("viewModelParticipants").getData().ShowFirstName = true;
				this.getView().getModel("viewModelParticipants").getData().ShowFullName = false;
				this.getView().getModel("viewModelParticipants").refresh(true);

			}
		},
		_onSortFilterGroupPress: function (oEvent) {

			this.mSettingsDialogs = this.mSettingsDialogs || {};
			var sSourceId = oEvent.getSource().getId();
			var oDialog = this.mSettingsDialogs["SortFilterGroupDialog"];

			var confirmHandler = function (oConfirmEvent) {
				var self = this;
				var sFilterString = oConfirmEvent.getParameter("filterString");
				var oBindingData = {};

				/* Grouping */
				if (oConfirmEvent.getParameter("groupItem")) {
					var sPath = oConfirmEvent.getParameter("groupItem").getKey();
					oBindingData.groupby = [new sap.ui.model.Sorter(sPath, oConfirmEvent.getParameter("groupDescending"), true)];
				} else {
					// Reset the group by
					oBindingData.groupby = null;
				}

				/* Sorting */
				if (oConfirmEvent.getParameter("sortItem")) {
					var sPath = oConfirmEvent.getParameter("sortItem").getKey();
					oBindingData.sorters = [new sap.ui.model.Sorter(sPath, oConfirmEvent.getParameter("sortDescending"))];
				}

				/* Filtering */
				oBindingData.filters = [];
				// The list of filters that will be applied to the collection
				var oFilter;
				var vValueLT, vValueGT;

				// Simple filters (String)
				var mSimpleFilters = {},
					sKey;
				for (sKey in oConfirmEvent.getParameter("filterKeys")) {
					var aSplit = sKey.split("___");
					var sPath = aSplit[1];
					var sValue1 = aSplit[2];
					var oFilterInfo = new sap.ui.model.Filter(sPath, "EQ", sValue1);

					// Creating a map of filters for each path
					if (!mSimpleFilters[sPath]) {
						mSimpleFilters[sPath] = [oFilterInfo];
					} else {
						mSimpleFilters[sPath].push(oFilterInfo);
					}
				}

				for (var path in mSimpleFilters) {
					// All filters on a same path are combined with a OR
					oBindingData.filters.push(new sap.ui.model.Filter(mSimpleFilters[path], false));
				}

				aCollections.forEach(function (oCollectionItem) {
					var oCollection = self.getView().byId(oCollectionItem.id);
					var oBindingInfo = oCollection.getBindingInfo(oCollectionItem.aggregation);
					var oBindingOptions = this.updateBindingOptions(oCollectionItem.id, oBindingData, sSourceId);
					if (oBindingInfo.model === "kpiModel") {
						oCollection.getObjectBinding().refresh();
					} else {
						oCollection.bindAggregation(oCollectionItem.aggregation, {
							model: oBindingInfo.model,
							path: oBindingInfo.path,
							parameters: oBindingInfo.parameters,
							template: oBindingInfo.template,
							templateShareable: true,
							sorter: oBindingOptions.sorters,
							filters: oBindingOptions.filters
						});
					}

					// Display the filter string if necessary
					if (typeof oCollection.getInfoToolbar === "function") {
						var oToolBar = oCollection.getInfoToolbar();
						if (oToolBar && oToolBar.getContent().length === 1) {
							oToolBar.setVisible(!!sFilterString);
							oToolBar.getContent()[0].setText(sFilterString);
						}
					}
				}, this);
			}.bind(this);

			function resetFiltersHandler() {

			}

			function updateDialogData(filters) {
				var mParams = {
					context: oReferenceCollection.getBindingContext(),
					success: function (oData) {
						var oJsonModelDialogData = {};
						// Loop through each entity
						oData.results.forEach(function (oEntity) {
							// Add the distinct properties in a map
							for (var oKey in oEntity) {
								if (!oJsonModelDialogData[oKey]) {
									oJsonModelDialogData[oKey] = [oEntity[oKey]];
								} else if (oJsonModelDialogData[oKey].indexOf(oEntity[oKey]) === -1) {
									oJsonModelDialogData[oKey].push(oEntity[oKey]);
								}
							}
						});

						var oDialogModel = oDialog.getModel();

						if (!oDialogModel) {
							oDialogModel = new sap.ui.model.json.JSONModel();
							oDialog.setModel(oDialogModel);
						}
						oDialogModel.setData(oJsonModelDialogData);

					}
				};
				var sPath;
				var sModelName = oReferenceCollection.getBindingInfo(aCollections[0].aggregation).model;
				// In KPI mode for charts, getBindingInfo would return the local JSONModel
				if (sModelName === "kpiModel") {
					sPath = oReferenceCollection.getObjectBinding().getPath();
				} else {
					sPath = oReferenceCollection.getBindingInfo(aCollections[0].aggregation).path;
				}
				mParams.filters = filters;
				oModel.read(sPath, mParams);
			}

			if (!oDialog) {
				oDialog = sap.ui.xmlfragment({
					fragmentName: "com.sap.build.standard.untitledPrototype.view.fragments.SortFilterGroupDialog"
				}, this);
				oDialog.attachEvent("confirm", confirmHandler);
				oDialog.attachEvent("resetFilters", resetFiltersHandler);

				this.mSettingsDialogs["SortFilterGroupDialog"] = oDialog;
			}

			this.mSettingsDialogs["SortFilterGroupDialog"].open();

			var aCollections = [];

			aCollections.push({
				id: "sap_List_Page_0-content-build_simple_Table-1599115124519",
				aggregation: "items"
			});

			var oReferenceCollection = this.getView().byId(aCollections[0].id);
			var oSourceBindingContext = oReferenceCollection.getBindingContext();
			var oModel = oSourceBindingContext ? oSourceBindingContext.getModel() : this.getView().getModel();

			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), oDialog);
			var designTimeFilters = this.mBindingOptions && this.mBindingOptions[aCollections[0].id] && this.mBindingOptions[aCollections[0].id]
				.filters && this.mBindingOptions[aCollections[0].id].filters[undefined];
			updateDialogData(designTimeFilters);

		},
		updateBindingOptions: function (sCollectionId, oBindingData, sSourceId) {
			this.mBindingOptions = this.mBindingOptions || {};
			this.mBindingOptions[sCollectionId] = this.mBindingOptions[sCollectionId] || {};

			var aSorters = this.mBindingOptions[sCollectionId].sorters;
			var aGroupby = this.mBindingOptions[sCollectionId].groupby;

			// If there is no oBindingData parameter, we just need the processed filters and sorters from this function
			if (oBindingData) {
				if (oBindingData.sorters) {
					aSorters = oBindingData.sorters;
				}
				if (oBindingData.groupby || oBindingData.groupby === null) {
					aGroupby = oBindingData.groupby;
				}
				// 1) Update the filters map for the given collection and source
				this.mBindingOptions[sCollectionId].sorters = aSorters;
				this.mBindingOptions[sCollectionId].groupby = aGroupby;
				this.mBindingOptions[sCollectionId].filters = this.mBindingOptions[sCollectionId].filters || {};
				this.mBindingOptions[sCollectionId].filters[sSourceId] = oBindingData.filters || [];
			}

			// 2) Reapply all the filters and sorters
			var aFilters = [];
			for (var key in this.mBindingOptions[sCollectionId].filters) {
				aFilters = aFilters.concat(this.mBindingOptions[sCollectionId].filters[key]);
			}

			// Add the groupby first in the sorters array
			if (aGroupby) {
				aSorters = aSorters ? aGroupby.concat(aSorters) : aGroupby;
			}

			var aFinalFilters = aFilters.length > 0 ? [new sap.ui.model.Filter(aFilters, true)] : undefined;
			return {
				filters: aFinalFilters,
				sorters: aSorters
			};

		},
		getCustomFilter: function (sPath, vValueLT, vValueGT) {
			if (vValueLT !== "" && vValueGT !== "") {
				return new sap.ui.model.Filter([
					new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.GT, vValueGT),
					new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.LT, vValueLT)
				], true);
			}
			if (vValueLT !== "") {
				return new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.LT, vValueLT);
			}
			return new sap.ui.model.Filter(sPath, sap.ui.model.FilterOperator.GT, vValueGT);

		},
		getCustomFilterString: function (bIsNumber, sPath, sOperator, vValueLT, vValueGT) {
			switch (sOperator) {
			case sap.ui.model.FilterOperator.LT:
				return sPath + (bIsNumber ? " (Less than " : " (Before ") + vValueLT + ")";
			case sap.ui.model.FilterOperator.GT:
				return sPath + (bIsNumber ? " (More than " : " (After ") + vValueGT + ")";
			default:
				if (bIsNumber) {
					return sPath + " (More than " + vValueGT + " and less than " + vValueLT + ")";
				}
				return sPath + " (After " + vValueGT + " and before " + vValueLT + ")";
			}

		},
		filterCountFormatter: function (sValue1, sValue2) {
			return sValue1 !== "" || sValue2 !== "" ? 1 : 0;

		},

		_onSearchFieldLiveChange: function (oEvent) {
			var sControlId = "sap_List_Page_0-content-build_simple_Table-1599115124519";
			var oControl = this.getView().byId(sControlId);

			// Get the search query, regardless of the triggered event ('query' for the search event, 'newValue' for the liveChange one, 'value' for the liveChange of SelectDialogs).
			var sQuery = oEvent.getParameter("query") || oEvent.getParameter("newValue") || oEvent.getParameter("value");
			var sSourceId = oEvent.getSource().getId();

			return new Promise(function (fnResolve) {
				var aFinalFilters = [];

				var aFilters = [];
				// 1) Search filters (with OR)
				if (sQuery && sQuery.length > 0) {
					sQuery = sQuery;

					aFilters.push(new sap.ui.model.Filter("Persno", sap.ui.model.FilterOperator.Contains, parseInt(sQuery)));
					aFilters.push(new sap.ui.model.Filter("Fornavn", sap.ui.model.FilterOperator.Contains, sQuery));
					aFilters.push(new sap.ui.model.Filter("Efternavn", sap.ui.model.FilterOperator.Contains, sQuery));
					//aFilters.push(new sap.ui.model.Filter("Fødselsdag", sap.ui.model.FilterOperator.Contains, sQuery));
					//aFilters.push(new sap.ui.model.Filter("Email", sap.ui.model.FilterOperator.Contains, sQuery));
					//aFilters.push(new sap.ui.model.Filter("Udløbsdato", sap.ui.model.FilterOperator.Contains, sQuery));
					
						aFilters = this.orFilters(aFilters, null);
				}
				
			
				
				

				aFinalFilters = aFilters.aFilters !== undefined > 0 ? aFilters : [];
				var oBindingOptions = this.updateBindingOptions(sControlId, {
					filters: aFinalFilters
				}, sSourceId);
				var oBindingInfo = oControl.getBindingInfo("items");
				if (!oBindingInfo.parameters) {
  oBindingInfo.parameters = {};
}
if (!oBindingInfo.parameters.custom) {
  oBindingInfo.parameters.custom = {};
}
oBindingInfo.parameters.custom.search = sQuery;


				if (oBindingInfo) {
					oControl.bindAggregation("items", {
						model: oBindingInfo.model,
						path: oBindingInfo.path,
						parameters: oBindingInfo.parameters,
						template: oBindingInfo.template,
						templateShareable: true,
						sorter: oBindingOptions.sorters
						//filters: oBindingOptions.filters
					});
				}
			}.bind(this)).catch(function (err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		
		orFilters: function (filters, tempFilter) {
            var filter;
            if (!tempFilter) {
                filter = new Filter({
                    filters: [
                        filters[0],
                        filters[1]
                    ],
                    and: false
                });
                filters.splice(0, 2);
            } else {
                filter = new Filter({
                    filters: [
                        tempFilter,
                        filters[0]
                    ],
                    and: false
                });
                filters.splice(0, 1)
            }
            if (filters.length === 0) return filter;
            return this.orFilters(filters, filter);
        },
		_onRowPress: function (oEvent) {
			var changes = this.getView().getModel().hasPendingChanges();
			if (changes) {

				var messageBoxText = "Du har ændret data, som du ikke gemt. Ønsker du at gemme?";

				MessageBox.show(messageBoxText, {
					icon: sap.m.MessageBox.Icon.INFORMATION,
					title: "Information",
					actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
					defaultAction: sap.m.MessageBox.Action.NO,
					styleClass: this.getView().$().closest(".sapUiSizeCompact").length ? "sapUiSizeCompact" : "",
					onClose: function (oAction, object) {

						if (oAction === sap.m.MessageBox.Action.YES) {
							this.getView().setBusy(true);
							this.getView().getModel().submitChanges({
								success: function (oData, response) {
									this.getView().setBusy(false);
									MessageBox.success("Data er gemt.");
								}.bind(this),
								error: function (error) {
									//this.errorCallBackShowInPopUp(error);
								}.bind(this)
							});

						} else {
							this.getView().getModel().resetChanges();
						}
					}.bind(this)
				});

			} else {

				var source = oEvent.getSource();

				var oBindingContext = oEvent.getSource().getBindingContext();

				source.addStyleClass("twobmSelectedRow");

				if (this.previouslyRow !== undefined) {
					this.previouslyRow.removeStyleClass("twobmSelectedRow");
				}

				this.previouslyRow = source;
				return new Promise(function (fnResolve) {

					this.doNavigate("Participant", oBindingContext, fnResolve, "", 1);
				}.bind(this)).catch(function (err) {
					if (err !== undefined) {
						MessageBox.error(err.message);
					}
				});
			}
		},

		_onExpandButtonPress: function () {
			var endColumn = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().columnsVisibility.endColumn;
			var isFullScreen = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().isFullScreen;
			var nextLayout;
			var actionsButtonsInfo = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().actionButtonsInfo;
			if (endColumn && isFullScreen) {
				nextLayout = actionsButtonsInfo.endColumn.exitFullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(2).layout;
			}
			if (!endColumn && isFullScreen) {
				nextLayout = actionsButtonsInfo.midColumn.exitFullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(1).layout;
			}
			if (endColumn && !isFullScreen) {
				nextLayout = actionsButtonsInfo.endColumn.fullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(3).layout;
			}
			if (!endColumn && !isFullScreen) {
				nextLayout = actionsButtonsInfo.midColumn.fullScreen;
				nextLayout = nextLayout ? nextLayout : "MidColumnFullScreen"
			}
			var pageName = this.oView.sViewName.split(".");
			pageName = pageName[pageName.length - 1];
			this.oRouter.navTo(pageName, {
				layout: nextLayout
			});

		},
		_onCloseButtonPress: function () {
			var endColumn = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().columnsVisibility.endColumn;
			var nextPage;
			var nextLevel = 0;

			var actionsButtonsInfo = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().actionButtonsInfo;

			var nextLayout = actionsButtonsInfo.midColumn.closeColumn;
			nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(0).layout;

			if (endColumn) {
				nextLevel = 1;
				nextLayout = actionsButtonsInfo.endColumn.closeColumn;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(1).layout;
			}

			var pageName = this.oView.sViewName.split(".");
			pageName = pageName[pageName.length - 1];
			var routePattern = this.oRouter.getRoute(pageName).getPattern().split("/");
			var contextFilter = new RegExp("^:.+:$");
			var pagePattern = routePattern.filter(function (pattern) {
				var contextPattern = pattern.match(contextFilter);
				return contextPattern === null || contextPattern === undefined;
			});

			var nextPage = pagePattern[nextLevel];
			this.oRouter.navTo(nextPage, {
				layout: nextLayout
			});

		},
		onInit: function () {

			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.attachRouteMatched(this.handleRouteMatched, this);
			this.oFclModel = this.getOwnerComponent().getModel("FclRouter");
			this.oFclModel.setProperty("/targetAggregation", "beginColumnPages");
			this.oFclModel.setProperty("/expandIcon", {});
			this.oView.setModel(new sap.ui.model.json.JSONModel({}), "fclButton");

		},
		onExit: function () {

			// to destroy templates for bound aggregations when templateShareable is true on exit to prevent duplicateId issue
			var aControls = [{
				"controlId": "sap_List_Page_0-content-build_simple_Table-1599115124519",
				"groups": ["items"]
			}];
			for (var i = 0; i < aControls.length; i++) {
				var oControl = this.getView().byId(aControls[i].controlId);
				if (oControl) {
					for (var j = 0; j < aControls[i].groups.length; j++) {
						var sAggregationName = aControls[i].groups[j];
						var oBindingInfo = oControl.getBindingInfo(sAggregationName);
						if (oBindingInfo) {
							var oTemplate = oBindingInfo.template;
							oTemplate.destroy();
						}
					}
				}
			}

		},

		countParticipantsAndUpdateSelected: function (oEvent) {
			var viewModelParticipants = this.getView().getModel("viewModelParticipants");
			viewModelParticipants.getData().ParticipantsCount = oEvent.getParameter("total");
			viewModelParticipants.refresh();
			var items = oEvent.getSource().getItems();

			var selectedAll = false;

			// we only have to check the first 20 as it reloads every time
			for (var i = 0; i < 20; i++) {
				if (items[i].getProperty("selected")) {
					selectedAll = true;
				} else {
					selectedAll = false;
					return;
				}
			}

			if (selectedAll) {
				this.getView().byId("sap_List_Page_0-content-build_simple_Table-1599115124519").selectAll(true);
			}

		},

		formatDate: function (date) {

			if (date !== null) {
				var day = date.getDate();
				day = day.toString().length < 2 ? "0" + day : day;
				var month = parseInt(date.getMonth()) + 1;
				month = month.toString().length < 2 ? "0" + month : month;
				var year = date.getFullYear();

				var string = day + "." + month + "." + year;
				return string;
			}

			return "";
		},

		sortColumn: function (oEvent) {
			var particpanType = oEvent.getSource().getCustomData()[0].getProperty("value");
			var path;
			var descending;
			var list = this.getView().byId("sap_List_Page_0-content-build_simple_Table-1599115124519");

			// for first timed pressed
			var sortLength = list.getBinding("items").aSorters.length;
			if (sortLength === 0 || this.lastPressed !== particpanType) {
				descending = false;
			} else {
				// check table if descending or ascending
				descending = list.getBinding("items").aSorters[0].bDescending ? false : true;
			}

			// path and last pressed set
			if (particpanType === "medarbejdernr") {
				path = "Persno";
				this.lastPressed = "medarbejdernr";
			} else if (particpanType === "fornavne") {
				path = "Fornavn";
				this.lastPressed = "fornavne";
			} else if (particpanType === "efternavn") {
				path = "Efternavn";
				this.lastPressed = "efternavn";
			} else if (particpanType === "email") {
				path = "Email";
				this.lastPressed = "email";
			} else if (particpanType === "fødselsdag") {
				path = "F%C3%B8dselsdag";
				this.lastPressed = "fødselsdag";
			}

			// reset participantTableModel
			this.createParticipantTableModel();

			// set icon
			var icon = descending ? "sap-icon://sort-descending" : "sap-icon://sort-ascending";
			var participantTableModel = this.getView().getModel("participantTableModel");

			if (this.lastPressed === "medarbejdernr") {
				participantTableModel.getData().MedarbejdernrIcon = icon;
			} else if (this.lastPressed === "fornavne") {
				participantTableModel.getData().FornavneIcon = icon;
			} else if (this.lastPressed === "efternavn") {
				participantTableModel.getData().EfternavnIcon = icon;
			} else if (this.lastPressed === "email") {
				participantTableModel.getData().EmailIcon = icon;
			} else if (this.lastPressed === "fødselsdag") {
				participantTableModel.getData().BirthdayIcon = icon;
			}
			participantTableModel.refresh(true);

			var oSorter = new Sorter({
				path: path,
				descending: descending
			});

			// manual sorting
			list.getBinding("items").sort(oSorter);

			this.adjustFrontendToMobileAndFirstColumnActive();

		},

		selectAll: function (oEvent) {
			//alert("hej");
		},
		
		createStudent: function() {
			var iNextLevel = 0;
			var oNextUIState = this.getOwnerComponent().getSemanticHelper().getNextUIState(iNextLevel);
			var sNextLayout = oNextUIState.layout;
			this.oRouter.navTo("CreateParticipant", {
				beginContext: undefined,
				midContext: undefined,
				endContext: undefined,
				layout: sNextLayout
			}, false);
			
			
		},
		isUserAKKP: function(role) {
			if(role === "KKP"){
				return true;
			}else{
				return false;
			}
		},
	});
}, /* bExport= */ true);