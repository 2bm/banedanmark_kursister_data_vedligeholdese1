sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/ui/layout/HorizontalLayout",
	"sap/ui/layout/VerticalLayout",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/Label",
	"sap/m/MessageToast",
	"sap/m/Text",
	"sap/m/TextArea",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/ui/core/format/DateFormat",
	"com/sap/build/standard/untitledPrototype/model/models"
], function (BaseController, MessageBox, Dialog2, Utilities, History, DateFormat, models) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.untitledPrototype.controller.CreateParticipant", {
		handleRouteMatched: function (oEvent) {
			var oParams = oEvent.getParameters();
			this.currentRouteName = oParams.name;
			var sContext;
			if (oParams.arguments.midContext) {
				sContext = oParams.arguments.midContext;
			} else {
				if (this.getOwnerComponent().getComponentData()) {
					var patternConvert = function (oParam) {
						if (Object.keys(oParam).length !== 0) {
							for (var prop in oParam) {
								if (prop !== "sourcePrototype") {
									return prop + "(" + oParam[prop][0] + ")";
								}
							}
						}
					};

					sContext = patternConvert(this.getOwnerComponent().getComponentData().startupParameters);
				}
			}
			var sContextModelProperty = "/midContext";
				this.getView().getModel().resetChanges();

			var givenContext = this.getView().getModel().createEntry("/ParticipantSet");

			this.getView().setBindingContext(givenContext);
			/*
						if (sContext) {

							var oPath = {
								path: "/", // + sContext,
								parameters: {}
							};

							var contextPath = '/'; //+ sContext;
							var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

							this.getView().bindObject(oPath);
							this.oFclModel.setProperty(sContextModelProperty, sContext);
							//this.getView().setBindingContext(givenContext);

							this.getView().setBindingContext(givenContext);
							this.getView().bindElement(contextPath);

						}*/

			var pageName = this.oView.sViewName.split('.');
			pageName = pageName[pageName.length - 1];

			if (pageName === this.currentRouteName) {
				this.oView.getModel('fclButton').setProperty('/visible', true);
			} else {
				this.oView.getModel('fclButton').setProperty('/visible', false);
			}

			// test

			if (oEvent.mParameters.arguments.layout && oEvent.mParameters.arguments.layout.includes('FullScreen')) {
				this.oFclModel.setProperty('/expandIcon/img', 'sap-icon://exit-full-screen');
				this.oFclModel.setProperty('/expandIcon/tooltip', 'Exit Full Screen Mode');
			} else {
				this.oFclModel.setProperty('/expandIcon/img', 'sap-icon://full-screen');
				this.oFclModel.setProperty('/expandIcon/tooltip', 'Enter Full Screen Mode');
			}

			this.setInputEvents();
			this.clearInputs();
		},
		
		clearInputs: function() {
			this.getView().byId("firstName").setValue("");
			this.getView().byId("lastName").setValue("");
			/*this.getView().byId("street").setValue("");
			this.getView().byId("town").setValue("");
			this.getView().byId("zipCode").setValue("");*/
			this.getView().byId("cprNumber").setValue("");
			this.getView().byId("email").setValue("");
			this.getView().byId("refNr").setValue("");
		},

		setInputEvents: function () {
			this.getView().byId("firstName").onsapenter = (function () {
				var firstName = this.getView().byId("firstName").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Fornavn", firstName);
				this.getView().byId("firstName").setValueState("None");
			}).bind(this);

			this.getView().byId("firstName").onsapfocusleave = (function () {
				var firstName = this.getView().byId("firstName").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Fornavn", firstName);
				this.getView().byId("firstName").setValueState("None");
			}).bind(this);

			this.getView().byId("lastName").onsapenter = (function () {
				var lastName = this.getView().byId("lastName").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Efternavn", lastName);
				this.getView().byId("lastName").setValueState("None");
			}).bind(this);

			this.getView().byId("lastName").onsapfocusleave = (function () {
				var lastName = this.getView().byId("lastName").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Efternavn", lastName);
				this.getView().byId("lastName").setValueState("None");
			}).bind(this);
			
/*
			this.getView().byId("street").onsapenter = (function () {
				var street = this.getView().byId("street").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Gade", street);
				this.getView().byId("street").setValueState("None");
			}).bind(this);

			this.getView().byId("street").onsapfocusleave = (function () {
				var street = this.getView().byId("street").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Gade", street);
				this.getView().byId("street").setValueState("None");
			}).bind(this);

			this.getView().byId("town").onsapenter = (function () {
				var town = this.getView().byId("town").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/By", town);
				this.getView().byId("town").setValueState("None");
			}).bind(this);

			this.getView().byId("town").onsapfocusleave = (function () {
				var town = this.getView().byId("town").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/By", town);
				this.getView().byId("town").setValueState("None");
			}).bind(this);

			this.getView().byId("zipCode").onsapenter = (function () {
				var zipCode = this.getView().byId("zipCode").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Postnr", zipCode);
				this.getView().byId("zipCode").setValueState("None");
			}).bind(this);

			this.getView().byId("zipCode").onsapfocusleave = (function () {
				var zipCode = this.getView().byId("zipCode").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Postnr", zipCode);
				this.getView().byId("zipCode").setValueState("None");
			}).bind(this);*/

			this.getView().byId("email").onsapenter = (function () {
				var email = this.getView().byId("email").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Email", email);
			}).bind(this);

			this.getView().byId("email").onsapfocusleave = (function () {
				var email = this.getView().byId("email").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Email", email);
			}).bind(this);

			this.getView().byId("refNr").onsapenter = (function () {
				var refNr = this.getView().byId("refNr").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Refnr", refNr);
				this.getView().byId("refNr").setValueState("None");
			}).bind(this);

			this.getView().byId("refNr").onsapfocusleave = (function () {
				var refNr = this.getView().byId("refNr").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Refnr", refNr);
				this.getView().byId("refNr").setValueState("None");
			}).bind(this);

			this.getView().byId("cprNumber").onsapenter = (function () {
				var cprNumber = this.getView().byId("cprNumber").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Cpr", cprNumber);
				this.getView().byId("cprNumber").setValueState("None");
			}).bind(this);

			this.getView().byId("cprNumber").onsapfocusleave = (function () {
				var cprNumber = this.getView().byId("cprNumber").getValue();
				var context = this.getView().getBindingContext();
				context.getModel().setProperty(context.sPath + "/Cpr", cprNumber);
				this.getView().byId("cprNumber").setValueState("None");
			}).bind(this);

		},

		_onExpandButtonPress: function () {
			var endColumn = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().columnsVisibility.endColumn;
			var isFullScreen = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().isFullScreen;
			var nextLayout;
			var actionsButtonsInfo = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().actionButtonsInfo;
			if (endColumn && isFullScreen) {
				nextLayout = actionsButtonsInfo.endColumn.exitFullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(2).layout;
			}
			if (!endColumn && isFullScreen) {
				nextLayout = actionsButtonsInfo.midColumn.exitFullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(1).layout;
			}
			if (endColumn && !isFullScreen) {
				nextLayout = actionsButtonsInfo.endColumn.fullScreen;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(3).layout;
			}
			if (!endColumn && !isFullScreen) {
				nextLayout = actionsButtonsInfo.midColumn.fullScreen;
				nextLayout = nextLayout ? nextLayout : 'MidColumnFullScreen'
			}
			var pageName = this.oView.sViewName.split('.');
			pageName = pageName[pageName.length - 1];
			this.oRouter.navTo(pageName, {
				layout: nextLayout
			});

		},
		_onCloseButtonPress: function () {
			var endColumn = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().columnsVisibility.endColumn;
			var nextPage;
			var nextLevel = 0;

			var actionsButtonsInfo = this.getOwnerComponent().getSemanticHelper().getCurrentUIState().actionButtonsInfo;

			var nextLayout = actionsButtonsInfo.midColumn.closeColumn;
			nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(0).layout;

			if (endColumn) {
				nextLevel = 1;
				nextLayout = actionsButtonsInfo.endColumn.closeColumn;
				nextLayout = nextLayout ? nextLayout : this.getOwnerComponent().getSemanticHelper().getNextUIState(1).layout;
			}

			var pageName = this.oView.sViewName.split('.');
			pageName = pageName[pageName.length - 1];
			var routePattern = this.oRouter.getRoute(pageName).getPattern().split('/');
			var contextFilter = new RegExp('^:.+:$');
			var pagePattern = routePattern.filter(function (pattern) {
				var contextPattern = pattern.match(contextFilter);
				return contextPattern === null || contextPattern === undefined;
			});

			var nextPage = pagePattern[nextLevel];
			this.oRouter.navTo(nextPage, {
				layout: nextLayout
			});

		},
		onInit: function () {

			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getRoute('CreateParticipant').attachMatched(this.handleRouteMatched, this);
			this.oFclModel = this.getOwnerComponent().getModel("FclRouter");
			this.oFclModel.setProperty('/targetAggregation', 'midColumnPages');
			this.oFclModel.setProperty('/expandIcon', {});
			this.oView.setModel(new sap.ui.model.json.JSONModel({}), 'fclButton');

		},

		onPressCreateStudent: function () {

			if (this.participantInformationValidates()) {
				this.getView().setBusy(true);
				this.getView().getModel().submitChanges({
					success: jQuery.proxy(function (oData, oResponse) {
						
						if (oData.__batchResponses[0].__changeResponses && oData.__batchResponses[0].__changeResponses[0].statusCode === '201'){
						
						this.getView().setBusy(false);
						window.history.go(-1);
						}
						else{
							this.errorCallBackShowInPopUp(oData.__batchResponses[0]);
					
						}
						this.getView().setBusy(false);
						//this.clearCreateWorkOrderModel();
					}, this),
					error: jQuery.proxy(function (error) {
						this.getView().setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}, this)
				});

			}
		},
		
	

		participantInformationValidates: function () {
			var validationError = false;

			//reset
			this.getView().byId("firstName").setValueState("None");
			this.getView().byId("lastName").setValueState("None");
			this.getView().byId("cprNumber").setValueState("None");
			//this.getView().byId("refNr").setValueState("None");

			if (!this.getView().getBindingContext().getObject().Fornavn) {
				this.getView().byId("firstName").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().getBindingContext().getObject().Efternavn) {
				this.getView().byId("lastName").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().getBindingContext().getObject().Cpr) {
				this.getView().byId("cprNumber").setValueState("Error");
				validationError = true;
			}


			if (validationError) {
				return false;
			}

			return true;
		},

		errorCallBackShowInPopUp: function (oError) {

			if (oError) {
			try{
					var errorCode = JSON.parse(oError.response.body).error.code;
					var errorMessage = JSON.parse(oError.response.body).error.innererror.errordetails[0].message;
}
				catch(err) {errorMessage = 'Fejl';}
			
					sap.m.MessageBox.error(errorMessage);
			
			}
		},

	});
}, /* bExport= */ true);